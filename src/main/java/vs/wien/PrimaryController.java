package vs.wien;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import vs.dao.DbDao;
import vs.model.Harcos;
import vs.view.Screen;

public class PrimaryController implements Initializable {
	ArrayList<Harcos> harcosok = new ArrayList<>();

	@FXML
	AnchorPane apUdv;
	@FXML
	Pane paneCsatater;
	@FXML
	TabPane tpWien;
	@FXML
	ImageView ivWien;
	@FXML
	ImageView ivVar;
	@FXML
	ListView<Harcos> lvHarcosok;
	@FXML
	Button btnCsata;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/wien", "root", "1234");
			DbDao dbDao = new DbDao(conn);
			int id = 1;
			String nev;
			String rang;
			while (dbDao.read(id) != null) {
				nev = dbDao.read(id).getNev();
				rang = dbDao.read(id).getRang();
				Harcos harcos = new Harcos(id, nev, rang);
				harcos.sorsol();
				harcosok.add(harcos);
				id++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Media media = new Media("file:///work/java/wien/feladat/matyas_mesek_focim_dala.mp3");
		MediaPlayer player = new MediaPlayer(media);
		player.play();
		Screen screen = new Screen(harcosok, paneCsatater, lvHarcosok);
		screen.refreshList();
		
		tpWien.setOnMouseClicked((new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {

				ivWien.fitHeightProperty().bind(apUdv.heightProperty());
				ivWien.fitWidthProperty().bind(apUdv.widthProperty());

				if (tpWien.getSelectionModel().getSelectedItem().getText().equals("Üdv...")) {
					player.play();
				}
				if (tpWien.getSelectionModel().getSelectedItem().getText().equals("Csata")) {
					player.stop();
				}
			}
		}));

		btnCsata.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("csata");
				for (Harcos harcos : harcosok) {
					harcos.setHarcol(false);
				}
				ObservableList<Harcos> csatazokSelected = lvHarcosok.getSelectionModel().getSelectedItems();
				for (Harcos harcos : csatazokSelected) {
					harcosok.get(harcosok.indexOf(harcos)).setHarcol(true);
				}
				lvHarcosok.getSelectionModel().clearSelection();

				screen.drawHarcosok();

			}
		});

		paneCsatater.setOnMouseClicked(new EventHandler<MouseEvent>() {
			Integer result;
			HandleFighters hf = new HandleFighters(harcosok);

			@Override
			public void handle(MouseEvent event) {
				Random rnd = new Random();
				double clickedX = event.getSceneX();
				double clickedY = event.getSceneY();
				result = hf.checkPosition(clickedX - 210, clickedY - 40);
				System.out.println("Result : " + result);
			 if (result != null) {
					if (rnd.nextBoolean()) { // meglőtték
						harcosok.get(result).meglottek();
					} else { // ő lő
						harcosok.get(result).lo();
					}
				} else {
					System.out.println("Nem talált...");
				}

				hf.sortHarcos();
				
				screen.refreshList();
				screen.drawHarcosok();
			}
		});
	}

}
