package vs.wien;

import java.util.ArrayList;

import vs.model.Harcos;

public class HandleFighters {

	public HandleFighters(ArrayList<Harcos> harcosok) {
		super();
		this.harcosok = harcosok;
	}

	ArrayList<Harcos> harcosok;

	
	public Integer checkPosition(double xPos, double yPos) { // ide kell vizsgálat , hogy csatázik e , él e stb
		// System.out.println(csatazok.get(0));
		Integer result = null;
		for (int i = 0; i < this.harcosok.size(); i++) {
			Harcos harcos = this.harcosok.get(i);
			if ((Math.abs(harcos.getxPos() - xPos) < 10) && (Math.abs(harcos.getyPos() - yPos) < 10)) {
				System.out.println("hit " + harcos);
				result = i;
			}
		}
		return result;
	}

	public void sortHarcos() {
		for (int j = 0; j < harcosok.size() - 1; j++) {
			if (harcosok.get(j).isDead()) {
				harcosok.remove(j);
			}
		}
		boolean changed = true;
		Harcos harcosTar;
		while (changed == true) {
			changed = false;
			for (int i = 0; i < harcosok.size() - 1; i++) {
				if (harcosok.get(i).getSebzes() < harcosok.get(i + 1).getSebzes()) {
					harcosTar = harcosok.get(i + 1);
					harcosok.remove(i + 1);
					harcosok.add(i, harcosTar);
					changed = true;
				}
			}
		}
	}

}
