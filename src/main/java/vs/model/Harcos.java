package vs.model;

import java.util.Random;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Harcos {

	int id;
	int sebzes = 0;
	String nev;
	String rang;
	boolean dead = false;
	boolean harcol = false;
	int xPos;
	int yPos;
	Circle circle = new Circle();

	public Harcos(int id, String nev, String rang) {
		super();
		this.id = id;
		this.nev = nev;
		this.rang = rang;
	}

	public void lo() {
		this.sebzes++;
	}

	public void meglottek() {
		if (this.sebzes > 1) {
			this.sebzes--;
		} else {
			this.sebzes = 0;
			this.dead = true;
			this.harcol= false;
		}

	}

	public void sorsol() {
		Random rnd = new Random();
		this.setxPos(rnd.nextInt(460) + 20);
		this.setyPos(rnd.nextInt(200) + 400);
		this.getCircle().setCenterX(this.getxPos());
		this.getCircle().setCenterY(this.getyPos());
		Color c = Color.rgb(rnd.nextInt(200) + 50, rnd.nextInt(200) + 50, rnd.nextInt(200) + 50);
		this.getCircle().setRadius(5);
		this.getCircle().setStroke(c);
		this.getCircle().setStrokeWidth(10);
		this.getCircle().setFill(Color.TRANSPARENT);
		this.getCircle().setVisible(false);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSebzes() {
		return sebzes;
	}

	public void setSebzes(int sebzes) {
		this.sebzes = sebzes;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getRang() {
		return rang;
	}

	public void setRang(String rang) {
		this.rang = rang;
	}

	public int getxPos() {
		return xPos;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public void setyPos(int yPos) {
		this.yPos = yPos;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
	
	public boolean isHarcol() {
		return harcol;
	}

	public void setHarcol(boolean harcol) {
		this.harcol = harcol;
	}

	public Circle getCircle() {
		return circle;
	}

	public void setCircle(Circle circle) {
		this.circle = circle;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
//		builder.append(id);
//		builder.append(" ");
		builder.append(nev);
		builder.append(" ");
		builder.append(dead);
		builder.append(" ");
		builder.append(harcol);
		builder.append(" ");
		builder.append(sebzes);
		builder.append(" ");
//		builder.append(getxPos());
//		builder.append(" ");
//		builder.append(getyPos());

		return builder.toString();
	}



}
