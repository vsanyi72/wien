package vs.view;

import java.util.ArrayList;
import java.util.function.Consumer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.Pane;
import vs.model.Harcos;

public class Screen {

	ArrayList<Harcos> harcosok;
	Pane paneCsatater;
	ListView<Harcos> lvHarcosok;

	public Screen(ArrayList<Harcos> harcosok, Pane paneCsatater, ListView<Harcos> lvHarcosok) {
		super();
		this.harcosok = harcosok;
		this.paneCsatater = paneCsatater;
		this.lvHarcosok = lvHarcosok;
	}

	public void refreshList() {
		ObservableList<Harcos> obHarcosok = FXCollections.observableList(harcosok);
		lvHarcosok.setItems(obHarcosok);
		lvHarcosok.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		lvHarcosok.refresh();
	}

	public void drawHarcosok() {
		for (Harcos harcos1 : harcosok) {
			//harcos1.getCircle().setVisible(true);
			this.paneCsatater.getChildren().remove(harcos1.getCircle());
		}

		for (Harcos harcos : harcosok) {
			if (!harcos.isDead() && (harcos.isHarcol())) {
				harcos.getCircle().setVisible(true);
				this.paneCsatater.getChildren().add(harcos.getCircle());
			}

		}
	}
}