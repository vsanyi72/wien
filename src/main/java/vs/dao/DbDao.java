package vs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import vs.model.Harcos;

public class DbDao implements Dao {

	Connection conn;
	String read;
	String readAll;
	String write;
	String update;

	public DbDao(Connection conn) {
		super();
		this.conn = conn;
		this.read = "SELECT * FROM katonak WHERE id = ?";
	}

	@Override
	public Harcos read(int id) throws Exception {
		Harcos h = null;
		
		try(PreparedStatement pst = this.conn.prepareStatement(this.read)){
			pst.setInt(1, id);
			
			ResultSet result = pst.executeQuery();
			
			while(result.next()) {
				h = new Harcos(
						result.getInt("id"),
						result.getString("nev"), //
						result.getString("rang"));
			}
			
		} catch (Exception e) {
			throw new SQLException("Hiba a result set kiolvasásában!", e);
		}
		return h;
	}

}
