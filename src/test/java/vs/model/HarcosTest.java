package vs.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HarcosTest {

	@Test
	void testObject() {
		Harcos harcos = new Harcos(3, "Bela", "ormester"); 
		assertNotNull(harcos);
		
	}
	
	@Test
	void testFunctions() {
		Harcos harcos = new Harcos(3, "Bela", "ormester"); 
		harcos.setSebzes(1);
		harcos.lo();
		assertEquals(2,harcos.getSebzes());
		harcos.meglottek();
		harcos.meglottek();
		assertEquals(true, harcos.isDead());
		
	}
	
	@Test
	void testCircle() {
		Harcos harcos = new Harcos(3, "Bela", "ormester"); 
		assertNotNull(harcos.getCircle());
		harcos.sorsol();
		assertEquals(5,harcos.getCircle().getRadius());
		
		
	}
	

}
